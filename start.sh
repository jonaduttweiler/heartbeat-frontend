#!/bin/bash

####################################################################################
###                        OPTIONS & ARGUMENTS                                   ###
####################################################################################
WEB_SOCKET_IP="" #10.1.15.162
WEB_SOCKET_PORT="" #3010
PORT=""

while true; do
  case "$1" in
    -i | --ws-ip ) WEB_SOCKET_IP=$2; shift 2;;
	-q | --ws-port ) WEB_SOCKET_PORT=$2; shift 2;;
    -p | --port ) PORT=$2; shift 2;;
    * ) break ;;
  esac
done

#comprobar que haya pasado los dos parametros 
if [ -z "${WEB_SOCKET_IP}" ] || [ -z "${WEB_SOCKET_PORT}" ] ; then
     echo "Faltan parametros para procesar la solicitud (--ws-ip y --ws-port son requeridos)"
     exit 1 #<- esto es fundamental
fi

WEB_SOCKET_URL="http://"${WEB_SOCKET_IP}":"${WEB_SOCKET_PORT}

####################################################################################
###                                   MAIN                                       ###
####################################################################################
#Crear un archivo temporal w/variable substitution, y copiarlo en target
BASEDIR=$(readlink -f "$(dirname "$0")") #absolut path

target_dir="${BASEDIR}/src/environments/environment.ts"

cat > config_test_file <<- EOM
export const environment = {  
  production: true,
  socket_server_url:'${WEB_SOCKET_URL}'
}
EOM

#validate ts. SOLAMENTE SI PASA EL TEST HACER EL REMPLAZO
mv config_test_file ${target_dir}



if [ -n "${PORT}" ]; then
	echo "Starting heartbeat frontend at ${PORT}..."
	ng serve --host 0.0.0.0 --port ${PORT}
else 
	echo "Especificar un puerto para el front end con la opcion --port"
fi