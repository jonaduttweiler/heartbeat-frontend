import { Component, OnInit, DoCheck ,Input} from '@angular/core';
import { trigger,state,style,animate,transition} from '@angular/animations';
import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

import { RequestStatus } from '../request-status.enum';
import { RequestFilter } from '../request-filter';
 
import { faCheck,faTimes,faExclamationTriangle,faExchangeAlt,faHourglassEnd,faStopwatch,faCircleNotch,faTimesCircle} from '@fortawesome/free-solid-svg-icons';


import { Request } from '../request';

@Component({
  selector: 'app-requests-table',
  templateUrl: './requests-table.component.html',
  styleUrls: ['./requests-table.component.css'],
  /**animations: [
    trigger('flyInOut', [
      state('in', style({transform: 'scaleY(1)'})),
      transition(':enter', [style({transform: 'scaleY(0)'}), animate(100) ]),
      transition(':leave', [animate(100, style({transform: 'scaleY(0)'})) ])
    ])
 ]*/
})
export class RequestsTableComponent implements OnInit, DoCheck {

	@Input() request_history: Request[];
  @Input() requests_filter: RequestFilter; 
  previous_length : number = 0;


  //Icons
  faCheck = faCheck ;
  faTimes = faTimes ;
  faExclamationTriangle = faExclamationTriangle ;
  faExchangeAlt = faExchangeAlt;
  faHourglassEnd = faHourglassEnd;
  faCircleNotch= faCircleNotch;
  faStopwatch = faStopwatch;
  faTimesCircle= faTimesCircle;

  //Expose enum to template through property
  RequestStatus = RequestStatus;

  constructor() { }

  ngOnInit() {
  }

  getRowStyle(request:Request):string{
  	let class_str:string;
  	switch(request.getStatus()) {
  		case RequestStatus.ok:{
  			class_str="table-success";
  			break;
  		}
  		case RequestStatus.sent:{
  			class_str="table-light";
  			break;
  		}
  		case RequestStatus.delayed:{
  			class_str="table-warning";
  			break;
  		}
  		case RequestStatus.redirect:{
  			class_str="table-warning";
  			break;
  		}
      case RequestStatus.cookies_redirect:{
        class_str="table-info";
        break;
      }
  		case RequestStatus.timeout:{
  			class_str="table-danger";
  			break;
  		}
  		case RequestStatus.error:{
  			class_str="table-danger";
  			break;
  		}
  		case RequestStatus.connection_problem:{
  			class_str="table-secondary";
  			break;
  		}
  	}
  	return class_str;
  	
  }


  trackByRequest(index: number, request: Request): number { return request.id; }

  ngDoCheck() {    
    const rqhl = this.request_history.length;
    if( rqhl != this.previous_length){
      this.previous_length = rqhl; 
      this.requests_filter = {...this.requests_filter};// TODO: REFACT. Not the most elegant way to update a view 
    }
  }

}
