import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { faBolt} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  @Input() socketOk:boolean;
  @Output("admin_event") admin_event = new EventEmitter();

  clicks_count : number = 0;
  is_admin: boolean = false;

  faBolt = faBolt;

  constructor() { }

  ngOnInit() {
  }


  web_socket_status():string{
    let msg: string;
    if(this.socketOk){
      msg = "Web socket connection ok";      
    } else {
      msg = "Web socket lost connection";
    }
    return msg;
  }

  //such a kind of easter egg
  check_admin($event){
    this.clicks_count++;
    if(this.clicks_count == 7) {
      this.is_admin = !this.is_admin;
      console.log("You're admin now!");  
      this.admin_event.emit(this.is_admin); 
    } else if(this.clicks_count == 8) {
      this.is_admin = !this.is_admin;
      this.clicks_count = 0;
      this.admin_event.emit(this.is_admin); 
    }  
   }

}
