import { RequestStatus } from './request-status.enum';

export class Request {

	private status:RequestStatus;
	private changed_cookies:String;

	constructor(
		public id: number,
		private start_time: Date, //	Efecctively the timestamp when the backend server makes the request																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																						
		private _application:string,
		private host:string,
		private path:string,
		private cookies:string,
		private status_n:string,
		private status_code:number,
		private time_elapsed_ms:number 
		){

		this.determineStatus();
	}

	// A kinda of cool constructor 
	//Analizar si esto puede crear algun tipo de problemas. Se agrego como metodo de clase 
	//debido a que js no permite overloading del constructor
	public static create(request):Request{

		return new Request(
              request.id,// 0,
              request.start_time,// new Date(),
              request.application,// "Sigae web logic",
              request.hostname,// "app.santafe.gov.ar",
              request.path,// "/sigae-web/",
              request.cookies,// "SIGAE-WEB-LOGICapp=.sigae-web-logic-app13",
              request.status,// "delayed",
              request.status_code,// 200,
              request.time_elapsed_ms,// 52
          );
	}

	/**
		Updates the request whit data about the response
	*/
	public update(request){
			this.status_n = request.status; 
            this.status_code = request.status_code;
            this.changed_cookies = request.changed_cookies;//TODO: IF REDIRECT 
            this.time_elapsed_ms = request.time_elapsed_ms;
            this.determineStatus();
	}



	public getId():Number{
		return this.id;
	}

	public getStatus():RequestStatus {
		return this.status;
	}

	public get application():string {
		return this._application;
	}

	public get hostname():string {
		return this.host;
	}

	public get statuscode():number{
		return this.status_code;
	}



	//TODO: Implement cookies to cookies str method
	public getCookiesStr():string {
		return JSON.stringify(this.cookies);
	}


	public updateStatus(status:string):void {
		switch(status){
			case "connection_lost": {
				this.status_n  = "connection_problem";
				this.status = RequestStatus.connection_problem;
				break;
			}
		}
	}

	/**
		Receives an status as string and updats status property to the properly enum
		Always need that status_n property has already set
	*/
	private determineStatus(){
		
		switch(this.status_n){
			case 'sent':case 'created':
				this.status = RequestStatus.sent;	
				break;
			case 'ok':case 'success': //consideramos que las cookies redirect se dan solo en buenas circunstancias?
				if(this.changed_cookies){
					this.status = RequestStatus.cookies_redirect;
				} else {
					this.status = RequestStatus.ok;
				}
				break;
			case 'delayed':
				this.status = RequestStatus.delayed;
				break;
			case 'redirect':
				this.status = RequestStatus.redirect;
				break;
			case 'timeout':
				this.status = RequestStatus.timeout;
				break;
			case 'error':
				this.status = RequestStatus.error;
				break;
			case 'connection_problem':
				this.status = RequestStatus.connection_problem;
				break;
		}
		
	}
  
}
