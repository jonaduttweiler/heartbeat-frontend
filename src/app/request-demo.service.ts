import { Injectable } from '@angular/core';
import { Request }from 'request';


@Injectable({
  providedIn: 'root'
})
export class RequestDemoService {

	requests : Request[] = [ 
          new Request(0,new Date(),"Sigae web logic","app.santafe.gov.ar","/sigae-web/","SIGAE-WEB-LOGICapp=.sigae-web-logic-app11",
            "sent",0,41),
          new Request(0,new Date(),"Sigae web logic","app.santafe.gov.ar","/sigae-web/","SIGAE-WEB-LOGICapp=.sigae-web-logic-app12",
            "ok",200,41),
          new Request(0,new Date(),"Sigae web logic","app.santafe.gov.ar","/sigae-web/","SIGAE-WEB-LOGICapp=.sigae-web-logic-app13",
            "delayed",200,52),
          new Request(0,new Date(),"Sigae web logic","app.santafe.gov.ar","/sigae-web/","SIGAE-WEB-LOGICapp=.sigae-web-logic-app14",
            "redirect",200,51),
          new Request(0,new Date(),"Sigae web logic","app.santafe.gov.ar","/sigae-web/","SIGAE-WEB-LOGICapp=.sigae-web-logic-app15",
            "timeout",0,512),
          new Request(0,new Date(),"Sigae web logic","app.santafe.gov.ar","/sigae-web/","SIGAE-WEB-LOGICapp=.sigae-web-logic-app16",
            "error",400,122),
           new Request(0,new Date(),"Sigae web logic","app.santafe.gov.ar","/sigae-web/","SIGAE-WEB-LOGICapp=.sigae-web-logic-app16",
            "connection_problem",0,4121)
    ];

  constructor() { }
    

}
