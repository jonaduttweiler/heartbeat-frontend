import { Component, OnInit, DoCheck ,Input, Output , EventEmitter} from '@angular/core';
import {RequestFilter} from '../request-filter';

@Component({
  selector: 'app-filter-options',
  templateUrl: './filter-options.component.html',
  styleUrls: ['./filter-options.component.css']
})
export class FilterOptionsComponent implements OnInit,DoCheck {

  @Input() is_admin:boolean;
  @Output("filterChange") filter_msChange = new EventEmitter();
  
  application : string = "";
  host : string = "";
  status_code : number;
  active_filter : boolean = true;

  previous_filter : RequestFilter = new RequestFilter("","",undefined,false);

  constructor() { }

  ngOnInit() {
  }

  getFilter(): RequestFilter {
  	return new RequestFilter(
  		 this.application,
		   this.host,
		   this.status_code,
  		 this.active_filter
		 );
  }


  ngDoCheck() {
  	let current_filter = this.getFilter();
  	let changes = 0;
  	if(this.previous_filter.application != current_filter.application){
  		changes++;
  	}
  	if(this.previous_filter.host != current_filter.host){
		  changes++;
  	}
  	if(this.previous_filter.status_code != current_filter.status_code){
		  changes++;
  	}
  	if(this.previous_filter.active_filter != current_filter.active_filter){
		  changes++;
  	}
  	
  	if(changes > 0){
		  this.previous_filter = current_filter; 
		  this.filter_msChange.emit(current_filter);	
  	}
  }
}


