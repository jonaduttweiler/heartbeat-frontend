import { Pipe, PipeTransform } from '@angular/core';
import { Request } from './request'
import { RequestFilter } from './request-filter';

@Pipe({
  name: 'tableFilter'
})
export class TableFilterPipe implements PipeTransform {

  transform(requests: Request[], filter:RequestFilter, defaultFilter:boolean): any {
  	if(!filter){
    	return requests;
  	}
  	if(filter.application === "" && filter.host === "" && filter.status_code === undefined){	
  		return requests;
  	}
    
	return requests
			.filter(request => filter.application? request.application.includes(filter.application): true)
			.filter(request => filter.host? request.hostname.includes(filter.host): true)
			.filter(request => filter.status_code? request.statuscode == filter.status_code: true)
  }
}
