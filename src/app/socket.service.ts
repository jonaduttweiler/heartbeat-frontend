import { environment } from '../environments/environment';
import { Injectable } from '@angular/core';
import { Observable, Observer  } from 'rxjs';
import * as socketIo from 'socket.io-client';

@Injectable({
  providedIn: 'root'
})
export class SocketService {

	private socket;

	public initSocket(): void {
		this.socket = socketIo(environment.socket_server_url);
	}

	public emit(message_type,payload){
		console.log(`[SOCKET] => ${message_type}`)
		this.socket.emit(message_type,payload);
	}

	public onEvent(event: string): Observable<any> {
        return new Observable<string>(observer => {
            this.socket.on(event,(data: any) => observer.next(data));
        });
	}
	
	constructor() {}
}
