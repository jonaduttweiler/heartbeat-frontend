export enum RequestStatus {
	sent,
	ok,
	delayed,
	redirect,
	timeout,
	error,
	connection_problem,
	cookies_redirect //TODO: Eliminar si no es necesario
}


