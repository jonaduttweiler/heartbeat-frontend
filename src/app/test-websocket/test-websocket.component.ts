import { Component, OnInit } from '@angular/core';
import { SocketService } from '../socket.service';


@Component({
  selector: 'app-test-websocket',
  templateUrl: './test-websocket.component.html',
  styleUrls: ['./test-websocket.component.css']
})
export class TestWebsocketComponent implements OnInit {
  
  received_messages : string [] = [];

  constructor(private socketService : SocketService) { }

  ngOnInit() {
  	this.initIoConnection();
  }

  private initIoConnection(): void {
    this.socketService.onEvent("response_test")
      .subscribe((data) => {
        console.log('how access to payload?');
        console.log(data);
        this.received_messages.push(data);
	});
  }

    //**This function formats an object as json*/
  format(message){
  	return JSON.stringify(message);
  }

  handleClick(){
  	console.log("handleClick");
  	this.socketService.emit("message_test",{"object":"test"});
  }

}
