import { Component, Input, ChangeDetectorRef } from '@angular/core';
import { trigger,state,style,animate,transition} from '@angular/animations';

import { SocketService } from './socket.service';
import { Request } from './request';

import { RequestStatus } from './request-status.enum';


declare type Permission = 'denied' | 'granted' | 'default';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  socketOk:boolean=false;
  request_history: Request[] = [];

  show_config_panel = false; 
  show_filter_panel = false; 
  delay_threshold_ms: number = 5000;
  max_history_entries: number = 500; //check at interval?
  is_admin: boolean = false;
  check_interval_ms : number; 

  requests_filter: Object;
  
  public permission: Permission;
  

  constructor(private socketService:SocketService,private cd:ChangeDetectorRef){
    //this.request_history = this.testRequests();
    this.initIoConnection();
    this.requestPermission();
  }

  private initIoConnection(): void {
  	this.socketService.initSocket();

    this.socketService.onEvent("connect")
      .subscribe(() => {
        console.log('[WEB SOCKET] connected');
        this.socketOk=true;
      });
      
    this.socketService.onEvent("disconnect")
      .subscribe(() => {
        console.log('[WEB SOCKET] disconnected');
        this.socketOk=false;
        //Todas las request que estan en sent no se van a poder actualizar por el web socket.
        //cambiarles el estado.
        this.request_history
          .filter((request:Request) => {return request.getStatus() === RequestStatus.sent})
          .forEach((request:Request) => {return request.updateStatus("connection_lost")});

	  });

    this.socketService
      .onEvent("request_update")
      .subscribe((request) => {      
         // Create or update
         let idx = this.request_history.findIndex((req) => req.getId() === request.id);

         if(idx > -1 ){//update a request that already exists

           //delayed requests
           if(request.time_elapsed_ms > this.delay_threshold_ms){
             request.status="delayed";
             this.notifyRequest(request,"Delay in instance");
           }

           //error requests
           if(request.status_code > 400){
            this.notifyRequest(request,"Error in instance"); 
           }


           this.request_history[idx].update(request);
         } else { //insert a new one //TODO: evaluate unshift performance or order the history          
           this.request_history.unshift(Request.create(request));

           this.remove_old_entries(); //keep history length a reasonable level
         }
    });

    this.socketService.emit("get_check_interval",{});

    this.socketService.onEvent("get_check_interval_awk")
      .subscribe((payload) => {
        console.log(`[CHECK INTERVAL] received: ${payload.check_interval_ms}`);
        this.check_interval_ms = payload.check_interval_ms;

      });    



  }

  handleMenuEvent($event){
    //console.log("Handle menu event!");
    //console.log($event);

    switch ($event) {
        case "deleteHistory":
          this.request_history = [];
          break;
        case "checkNow":
          this.checkNow();
           break;
        case "pause":
          //emit pause through web socket
          this.checkNow();
           break;
        case "showWarning":
          //emit only send warnings through web socket
           break;
        case "configuration":
          //show filter menu
          this.show_config_panel = !this.show_config_panel;
           break;
        case "filter":
          //show configuration menu
          this.show_filter_panel = !this.show_filter_panel;
          break;
        
        default:
          // code...
          break;
      }  
  }


  update_filter($filter){
    this.requests_filter = $filter;
    this.cd.detectChanges();
  }

  private checkNow(): void {
    this.socketService.emit('check_now',{});
  }
  private pause(): void {
    this.socketService.emit('pause',{});
  }


  private remove_old_entries():void{
        let request_history_max_entries = 300; //TODO: PASAR ESTO A UN PARAMETRO DE LA APLICACION
        let request_history_remove_delta = 200; //TODO: PASAR ESTO A UN PARAMETRO DE LA APLICACION
        if(this.request_history.length > request_history_max_entries){
          console.log(`[DEBUG] delete last ${request_history_remove_delta} entries of history`);
          this.request_history.length -= request_history_remove_delta ;//delete the lasts. 
        }
  }

  update_delay_threshold($event){
    this.delay_threshold_ms = $event.target.value;
  }

  update_check_interval($event){
    let val = $event.target.value;

    if(val < 10000){
      return false;
    }

    if(this.is_admin){
         this.socketService.emit('set_check_interval',{
           is_admin: this.is_admin,
           ms_interval: val
         });   
    } else {
      console.log("You aren't admin");
    }
  }

  handle_change_admin_event($event){
    console.log("change admin level!");
    this.is_admin = $event;
  }



//Notifications
  isSupported(): boolean {
     return 'Notification' in window;
    }

  requestPermission(): void {
      let self = this;
      if ('Notification' in window) {
          Notification.requestPermission(function(status) {
              return self.permission = status;
          });
      }
  }

   notifyRequest(request,title): void {
       if(this.permission == "granted"){
        new Notification(title,{
              body:`Application: ${request.application}\nHost: ${request.hostname}\nStatus code: ${request.status_code}\nTime elapsed [ms]: ${request.time_elapsed_ms} `,
              icon:"../assets/images/notification_icon.png"
            }
          );
       } 
    };

}
