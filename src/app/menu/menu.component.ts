import { Component, OnInit , EventEmitter, Input, Output} from '@angular/core';
import { faCoffee, faSyncAlt, faPause, faTrash, faFire, faFilter, faCogs} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  
  @Input() socketOk:boolean;
  @Output() menuEventRequest = new EventEmitter<string>();

  faSyncAlt = faSyncAlt;
  faPause = faPause;
  faTrash = faTrash;
  faFire = faFire;
  faFilter = faFilter;
  faCogs = faCogs;
 
  constructor() { }

  ngOnInit() {
  }

  check_now($event){
    this.menuEventRequest.emit("checkNow");
  }
  pause($event){
    this.menuEventRequest.emit("pause");
  }
  deleteHistory($event){
    this.menuEventRequest.emit("deleteHistory");
  }
  showWarning($event){
    this.menuEventRequest.emit("showWarning");
  }
  filter($event){
    this.menuEventRequest.emit("filter");
  }
  configuration($event){
    this.menuEventRequest.emit("configuration");
  }
}
