import { Component, OnInit, Input, Output , EventEmitter} from '@angular/core';


@Component({
  selector: 'app-config-options',
  templateUrl: './config-options.component.html',
  styleUrls: ['./config-options.component.css']
})
export class ConfigOptionsComponent implements OnInit {

  @Input() is_admin:boolean;
  @Input() check_interval_ms:number;
  @Input() delay_threshold_ms:number;

  @Output("delay_threshold_msChange") ev_delay_threshold_msChange = new EventEmitter();
  @Output("check_interval_msChange") ev_check_interval_msChange = new EventEmitter();


  constructor() { }

  ngOnInit() {

  } 

  delay_threshold_msChange($event){
  	//console.log("Threshold value has changed!");
  	this.ev_delay_threshold_msChange.emit($event);	
  }

  check_interval_msChange($event){
    //console.log("Threshold value has changed!");
    this.ev_check_interval_msChange.emit($event);  
  }



}
