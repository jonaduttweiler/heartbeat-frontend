import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FilterOptionsComponent } from './filter-options/filter-options.component';
import { ConfigOptionsComponent } from './config-options/config-options.component';
import { RequestsTableComponent } from './requests-table/requests-table.component';
import { TestWebsocketComponent } from './test-websocket/test-websocket.component';
import { MenuComponent } from './menu/menu.component';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { FormsModule } from '@angular/forms';
import { TableFilterPipe } from './table-filter.pipe';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FilterOptionsComponent,
    ConfigOptionsComponent,
    RequestsTableComponent,
    TestWebsocketComponent,
    MenuComponent,
    TableFilterPipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
